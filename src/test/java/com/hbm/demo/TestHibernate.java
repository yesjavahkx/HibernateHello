package com.hbm.demo;

import com.hbn.demo.domain.Person;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.Serializable;

public class TestHibernate {
    private SessionFactory sessionFactory;

    @Before
    public void before() {
        Configuration configuration = new Configuration().configure("Hibernate.cfg.xml");
        sessionFactory = configuration.buildSessionFactory();//Hibernate 5 不过时!!
    }

    @After
    public void after() {
        sessionFactory.close();
    }

    //保存一个Person 对象
    @Test
    public void test1() {
        Person person = new Person().setName("Hkx").setPersonNo("傻B001").setSex(true);
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(person);
        session.getTransaction().commit();
        session.close();
    }

    //查询一个Person 对象
    @Test
    public void test2() {
        Session session = sessionFactory.openSession();
        Person person = session.get(Person.class, 1);
        System.out.println(person);
        session.close();
    }

    /**
     * 演示 Hibernate 的对象的状态,这是一个很无聊但是很重要的概念
     */
    @Test
    public void test3() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Person person = new Person().setName("lala").setPersonNo("牛B").setSex(true);//瞬态
        session.save(person);
        person.setName("HaHa");
      //  session.update(person);//显式更改,更新
        session.getTransaction().commit();
        session.close();
        //session 关闭后,数据库存在记录,但是没有加载到内容存中.托管
    }


    /**
     * 演示一些常save() ,saveOrUpdate() ,  persist() 的用法和差异
     */
    @Test
    public void test4() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Person pp = new Person().setName("哥哥").setPersonNo("gege").setSex(false);//一个女性的哥哥

       /* Serializable id = session.save(pp);
        System.out.println(id);*/
       // session.persist(pp);
      //  pp.setId(6);
        session.saveOrUpdate(pp);


        session.getTransaction().commit();
        session.close();
    }

    /**
     * 演示一些常
     * get() : 非延迟加载,get 方法执行时候就发送sql语句
     * load()  : 延迟加载,什么时候需要才发送sql语句
     *   1. 使用对象时不能关闭Session
     *   2. 查询对象不能是null
     */
    @Test
    public void test5() {
        Session session = sessionFactory.openSession();
        Person person = session.get(Person.class, 66);//返回一个代理对象,被没有去查询数据库
        System.out.println(person);//什么时候需要用到对象才去发送sql 语句
        session.close();
    }


    /**
     *
     * update() : 非延迟加载,get 方法执行时候就发送sql语句
     * load()  : 延迟加载,什么时候需要才发送sql语句
     *   1. 使用对象时不能关闭Session
     *   2. 查询对象不能是null
     */
    @Test
    public void test6() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Person person = new Person().setName("小姐姐").setSex(false).setId(4);
        Person person1 = (Person) session.merge(person);// 更改的同时返回一个持久态的对象!!
        person1.setName("小妹妹");//
//        session.update(person1);不需要显式更新!!
        session.getTransaction().commit();
        session.close();

    }
}
