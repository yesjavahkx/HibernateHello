package com.hbn.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "tb_person")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;//hibernate 开发Id 是核心!!
    @Column(name = "pname",length = 30,nullable = false)
    private String name;
    private String personNo;
    //@Transient//映射的时候忽略某个属性!
    @Column(columnDefinition = "int")
    private boolean sex;

    public Person() {
    }

    public Person(Integer id, String name, String personNo, boolean sex) {
        this.id = id;
        this.name = name;
        this.personNo = personNo;
        this.sex = sex;
    }

    public Integer getId() {
        return id;
    }

    public Person setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;
        return this;
    }

    public String getPersonNo() {
        return personNo;
    }

    public Person setPersonNo(String personNo) {
        this.personNo = personNo;
        return this;
    }

    public boolean isSex() {
        return sex;
    }

    public Person setSex(boolean sex) {
        this.sex = sex;
        return this;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", personNo='" + personNo + '\'' +
                ", sex=" + sex +
                '}';
    }
}
