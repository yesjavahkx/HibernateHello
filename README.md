# Hibernate 入门程序
1. 搭建maven 环境
    * 设置setting.xml ,目的是换maven 远程仓库的镜像.
2. 把项目换成maven 项目
    * 项目中右键--> add framework support ---> maven
    * 复制粘贴所提供的pom文件
    * 修改项目的gav
3. 开始写HelloWorld 代码
    * 写实体类
    * 写Hibernate.cfg.xml
    * 将实体类引入Hibernate.cfg.xml 中
    * 在实体类中添加对应的注解,完成基本的映射关系
        * @Entity :  必须,标志一个实体
        * @Table(name = "tb_Person") :可选,默认就是对应的表和实体类名一样
        * @Id : 必须
        * @GeneratedValue(strategy = GenerationType.IDENTITY): 生成方式
        * @Column(name ="firstName")  :指定列名,长度,非空,唯一等
        * @Transient : 忽略某一个属性
4. 理解Hibernate的几个核心概念
    * 瞬态: 内存存在对象,但是数据库没有
    * 持久态: 内存有,数据库也有,且已经关联.
    * 托管态: 数据库有记录,内存中没有.
5. 掌握几个常用的方法和差异
    * save()
    * saveOrUpdate()
    * persist() (推荐)
    * load() 
        * 延迟加载,什么时候需要才去查数据库
        * 使用之前,session不能关闭,否则报错
        * 不能加载不存在的记录
    * get() (推荐)
        * 不延迟加载,get 的时候就查数据库
        *  session关闭后,依然可以使用
        *  查询不存在的记录,会返回null
    * update() (推荐)
    * merge()